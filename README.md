# 1. MSC in Computer Engineering and Artificial Intelligence Project
This project researches the use of GANs to synthesise new child audio data as a data augmentation strategy for training TTS models that can interact with children.

The project researches the use of [ParallelWaveGAN](https://arxiv.org/abs/1910.11480) and [WaveGAN & SpecGAN](https://arxiv.org/abs/1802.04208), and uses their GitHub implementation projects as a basis: [ParallelWaveGAN GitHub](https://github.com/kan-bayashi/ParallelWaveGAN) and [WaveGAN GitHub](https://github.com/chrisdonahue/wavegan), respectively.

The training dataset for this project was not included due to its size, but can be provided upon request.

The two folders for these projects have their own original README documentation from their authors, useful for setup. As an extension, Dockerfiles have been included in each folder to reproduce the training results created in this project.

The thesis of this project, detailing the project with insights found from the results, can be accessed in the root of this project.

# 2. Training Results

## 1. ParallelWaveGAN 

- 100,000 epochs:
  <br />
  ![Ground truth audio](project_training_results/parallelwavegan/train1/audio_samples/100000steps/1_ref.wav)
  ![Reconstructed audio](project_training_results/parallelwavegan/train1/audio_samples/100000steps/1_gen.wav)
  ![alt text](project_training_results/parallelwavegan/train1/audio_samples/100000steps/1.png "100000 epochs ParallelWaveGAN")

- 200,000 epochs:
  <br />
  ![Ground truth audio](project_training_results/parallelwavegan/train1/audio_samples/200000steps/1_ref.wav)
  ![Reconstructed audio](project_training_results/parallelwavegan/train1/audio_samples/200000steps/1_gen.wav)
  ![alt text](project_training_results/parallelwavegan/train1/audio_samples/200000steps/1.png "200000 epochs ParallelWaveGAN")

- 300,000 epochs:
  <br />
  ![Ground truth audio](project_training_results/parallelwavegan/train1/audio_samples/300000steps/1_ref.wav)
  ![Reconstructed audio](project_training_results/parallelwavegan/train1/audio_samples/300000steps/1_gen.wav)
  ![alt text](project_training_results/parallelwavegan/train1/audio_samples/300000steps/1.png "300000 epochs ParallelWaveGAN")

- 400,000 epochs (max):
  <br />
  ![Ground truth audio](project_training_results/parallelwavegan/train1/audio_samples/400000steps/3_ref.wav)
  ![Reconstructed audio](project_training_results/parallelwavegan/train1/audio_samples/400000steps/3_gen.wav)
  ![alt text](project_training_results/parallelwavegan/train1/audio_samples/400000steps/3.png "400000 epochs ParallelWaveGAN")

## 2. WaveGAN

- WaveGAN
  - ground truth training examples for 2 seconds output
    <br />
    ![1](project_training_results/wavegan/wavegan_experiments/results_2sec/real1.wav)
    <br />
    ![2](project_training_results/wavegan/wavegan_experiments/results_2sec/real1.wav)

  - 2 seconds output samples
    <br />
    ![Epoch 29574](project_training_results/wavegan/wavegan_experiments/results_2sec/audio29574.wav)
    <br />
    ![Epoch 62589](project_training_results/wavegan/wavegan_experiments/results_2sec/audio62589.wav)
    <br />
    ![Epoch 114739](project_training_results/wavegan/wavegan_experiments/results_2sec/audio114739.wav)

  - ground truth training examples for 4 seconds output
    <br />
    ![1](project_training_results/wavegan/wavegan_experiments/results_4sec/real1.wav)
    <br />

  - 4 seconds output samples
    <br />
    ![Epoch 96610](project_training_results/wavegan/wavegan_experiments/results_4sec/audio96610.wav)
    <br />
    ![Epoch 159246](project_training_results/wavegan/wavegan_experiments/results_4sec/audio159246.wav)

- SpecGAN (1 sec only)
  - raw ground truth training examples
    <br />
    ![1](project_training_results/wavegan/specgan_experiments/results_specgan1/real1.wav)
    <br />
  - ground truth training examples (raw audio to spectrogram to audio converted)
    <br />
    ![1](project_training_results/wavegan/specgan_experiments/results_specgan1/x_ground_truth_after_spectrogram1.wav)
    <br />
  - output samples
    <br />
    ![Epoch 51651](project_training_results/wavegan/specgan_experiments/results_specgan1/audio51651_1.wav)
    <br />

