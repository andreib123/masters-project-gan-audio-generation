# utility script for copying all files with a particular extension in leaf subdirs to another dir

import glob
import shutil
import os, os.path


# paths for root and destination directories
base_path = "/home/local/CORP/abarcovschi/datasets/"
src_path = base_path + "myst/train-clean-100" # source dir with subdir structure
dst_path = base_path + "pwg_myst_clean" # destination dir
extension = ".wav" # the type of files to copy

# create required destination dir
os.makedirs(dst_path, exist_ok=True)

wavs = [] # filepaths of all .wav files in source directory file structure
# loop through all leaf subdirectories
for root, dirs, files in os.walk(src_path, topdown=False):
    if not dirs: # if no more subdirectories
        # append all .wav files in this leaf subdirectory to the list
        wavs += [os.path.join(root, file) for file in files if file.endswith(extension)]

print(f"The number of {extension} files to copy: {len(wavs)}")
# copy all .wav files to destination directory
for f in wavs:
    shutil.copy(f, dst_path)
